<?
$mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

include('../common/login/userinfo.php');
if (!$user->id_str == "1873618208") {
    header("Location: https://anime.spotlight.tokyo/");
}
include('../common/html/head.php');
?>
<style>
    footer .info {
        display: none;
    }
    h1 {
        text-align: center;
        background-color: #95e1d3;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 300px;
        color: white;
        margin-bottom: 0;
    }
    .container.content {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
    }
    #notes {
        overflow-x: scroll;
        -webkit-overflow-scrolling: touch;
        line-height: 1.1;
        letter-spacing: 0.2mm;
    }
    .d-flex {
        display: inline-block;
        white-space: normal;
        position: relative;
        padding: 0;
    }
    #notes .notetitle {
        min-width: 150px;
        height: 200px;
        min-width: 200px;
        position: relative;
        background-color: #95e1d3;
        background-size: cover!important;
        background-position: center!important;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
    }
    #notes .notetitle .title {
        color: white;
        font-size: 1.7rem;
        padding: 20px;
    }
    #notes .notetitle .title .subtitle {
        color: white;
        font-size: .8rem;
        margin-top: 5px;
        
    }
    #notes .notetitle .sub {
        position: absolute;
        font-size: .8rem;
        text-align: right;
        top: 0;
        left: 0;
        padding: 5px;
        background-color: #fce38a;
        color: white;
    }
    #notes .notetitle .time {
        position: absolute;
        top: 0;
        right: 0;
        padding: 5px;
        background-color: white;
        color: black;
    }
</style>
<h1>Dashboad</h1>
<div class="container content">
    <h3>Notes.</h3>
</div>
<div id="notes">
    <div class="container">
        <div class="d-flex flex-nowrap bold">
        <?
        $result = $mysqli->query("SELECT * FROM note order by note_id asc");
        while( $data = $result->fetch_assoc() ) {
            echo '<a style="text-decoration: none" href="https://anime.spotlight.tokyo/dashboad/edit/note?id='.$data['note_id'].'&category=note">';
            echo '<div class="notetitle">';
            echo '<div class="title">';
            echo $data['title'];
            echo '</div>';
            echo '<div class="time">';
            echo date('m月d日',strtotime($data['updated_time']));
            echo '</div>';
            echo '</div>';
            echo '</a>';
        }
        ?>
        </div>
    </div>
</div>
<div class="content container">
    <h3>Animes.</h3>
</div>
<div id="notes">
    <div class="container">
        <div class="d-flex flex-nowrap bold">
        <?
        $result = $mysqli->query("SELECT * FROM title order by anime_id asc");
        while( $data = $result->fetch_assoc() ) {
            echo '<a style="text-decoration: none" href="https://anime.spotlight.tokyo/dashboad/edit/note?id='.$data['anime_id'].'&category=anime">';
            echo '<div class="notetitle">';
            echo '<div class="title">';
            echo $data['title'];
            echo '</div>';
            echo '<div class="time">';
            echo $data['anime_id'];
            echo '</div>';
            echo '</div>';
            echo '</a>';
        }
        ?>
        </div>
    </div>
</div>
<?
include('../common/html/footer.php');
?>