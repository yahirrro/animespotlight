<?
$mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
if($_GET['category'] == 'note') {
    $result = $mysqli->query("SELECT * FROM note WHERE note_id = '".$_GET['id']."'");
    while( $data = $result->fetch_assoc() ) {
        $title = $data['title'];
        $body = $data['body'];
        $id = $data['note_id'];
        $updated_time = $data['updated_time'];
    }
}
elseif($_GET['category'] == 'anime') {
    $result = $mysqli->query("SELECT * FROM anime WHERE anime_id = '".$_GET['id']."'");
    while( $data = $result->fetch_assoc() ) {
        $title = $data['comment_title'];
        $body = $data['review'];
        $id = $data['anime_id'];
        $comment = $data['comment_msg'];
        $updated_time = $data['updated_time'];
    }
}
if (!$id) {
    $id = $_GET['id'];
}

include('../../common/login/userinfo.php');
if ($user->id_str == "1873618208") {}
else {
	header("Location: https://anime.spotlight.tokyo/");
}
include('../../common/html/head.php');
?>
<script src="../../common/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector:'#editform',
        language: "ja",
        height: 300,
        plugins: "code autosave",
        autosave_interval: "20s",
        content_css : 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css,https://anime.spotlight.tokyo/common/html/artwork.css',
        mobile: { theme: 'mobile' }
    })
</script>
<style>
    #title {
        color: white;
        letter-spacing: 0.1mm;
    }
    #title h1 {
        height: 300px;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
        background-color: #95e1d3;
        margin: 0;
    }
    #title .container {
        margin-top: -20px;
        height: 20px;
        font-size: 12px;
        text-align: right;
    }
    #content {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
    }
</style>
<div id="title">
    <h1><?= $title ?></h1>
    <div class="container">
        <div class="postdata">
            <?= date("Y年m月d日 H時i分",strtotime($updated_time)) ?>
        </div>
    </div>
</div>
<div class="container" id="content">
    <form action="submit.php" method="post">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label>カテゴリー</label>
                    <input name="category" class="form-control" type="text" placeholder="Default input" value="<?
                        if($_GET['category'] == 'note') {
                            echo 'note';
                        }
                        elseif($_GET['category'] == 'anime') {
                            echo 'anime';
                        }
                    ?>">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label>ID</label>
                    <input name="id" class="form-control" type="text" placeholder="Default input" value="<?= $id ?>" >
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>タイトル</label>
            <input name="title" class="form-control" type="text" placeholder="Default input" value="<?= $title ?>">
        </div>
        <?
            if($_GET['category'] == 'anime') {
                echo '<div class="form-group">
                <label>コメント</label>
                <input name="comment" class="form-control" type="text" placeholder="Default input" value="'.$comment.'">
                </div>';
            }
        ?>
        <div class="form-group">
            <label>本文</label>
            <textarea name="body" class="form-control" id="editform">
                <?= $body ?>
            </textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<?
include('../../common/html/footer.php');
?>