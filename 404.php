<?
session_start();
require_once 'common/login/common.php';
require_once 'common/login/twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$user = $connection->get("account/verify_credentials");

$pagetitle ='404 - アニメデイズ!';
include('common/html/head.php');
?>
<meta http-equiv="refresh" content="8;URL=https://anime.spotlight.tokyo">
<style>
    .site,footer {
        display: none;
    }
    body {
        background-color: black;
        color: white;
        
    }
    @keyframes fadeIn {
        0% {
            opacity:0;
        }
        10% {
            opacity: 0.9;
        }
        11% {
            opacity: 0;
        }
        12% {
            opacity: 0.9;
        }
        17% {
            opacity: 0;
        }
        50% {
            opacity: 0.1;
        }
        100% {
            opacity:1;
        }
    }
    @keyframes textIn {
        0% {
            opacity: 0;
        }
        30% {
            opacity: 0;
        }
        100% {
            opacity:1;
        }
    }
    #content {
        background-color: black;
        width: 100vw;
        height: 100vh;
        position: relative;
    }
    .text {
        text-align: center;
    }
    .text img {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        animation-name: fadeIn;
        animation-duration: 5s;
    }
    .text h1 {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        margin: auto;
        padding-bottom: 20px;
        animation-name: textIn;
        animation-duration: 10s;
        padding: 20px;
    }
    h1 {
        font-size: 1.5rem!important;
    }
</style>
<div id="content">
    <div class="text">
        <a href="https://anime.spotlight.tokyo/"><img src="common/picture/light.svg" width="100px" alt=""></a>
        <h1>お探しのページは見当たりませんでした。</h1>
    </div>
    
</div>

<?
include('common/html/footer.php');
?>