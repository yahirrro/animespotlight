<?
session_start();
require_once 'common/login/common.php';
require_once 'common/login/twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$user = $connection->get("account/verify_credentials");

$mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$result = $mysqli->query("SELECT * FROM note WHERE note_id = '".$_GET['id']."'");
while( $data = $result->fetch_assoc() ) {
    $title = $data['title'];
    $body = $data['body'];
    $updated_time = $data['updated_time'];
}

$pagetitle = $title.' - アニメデイズ!';

if(!$title) {
    echo '<meta http-equiv="refresh" content="0; URL=https://anime.spotlight.tokyo/404.php">';
}

include('common/html/head.php');
include('common/html/ad.php');

?>
<style>
    #title {
        color: white;
        letter-spacing: 0.1mm;
    }
    #title h1 {
        height: 300px;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #95e1d3;
        margin: 0;
    }
    #title .container {
        margin-top: -20px;
        height: 20px;
        font-size: 12px;
        text-align: right;
    }
    #content {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
    }
</style>
<div id="title">
    <h1><?= $title ?></h1>
    <div class="container">
        <div class="postdata">
            <?= date("Y年m月d日 H時i分",strtotime($updated_time)) ?>
        </div>
    </div>
</div>
<?= $ad;?>
<div id="content" class="container">
    <?= $body ?>
</div>
<?= $ad;?>
<?
include('common/html/footer.php');
?>