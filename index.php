<?
session_start();
require_once 'common/login/common.php';
require_once 'common/login/twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$user = $connection->get("account/verify_credentials");



$mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$season = '2017w';
$pagetitle = '今期オススメのアニメリスト - アニメデイズ!';

include('common/OpenGraph.php');
include('common/html/head.php');
include('common/html/ad.php');
?>
	<style>
        
        #title {
            position: relative;
            min-height: 230px;
            width: 100%;
            background-color: #95e1d3;
            color: white;
            font-size: 2rem;
            font-weight: 900;
        }
        #title .toptext {
            position: absolute;
            bottom:0;
            right: 0;
            left: 0;
            padding: 0 10px;
            padding-bottom: 30px;
            font-size: 1rem;
            text-align: center;
            text-shadow: 0 10px 20px rgba(0,0,0,0.15),0 10px 40px rgba(0,0,0,0.10);
        }
        #title .toptext h1 {
            font-size: 1.8rem;
            display: inline-block;
            background-image: linear-gradient(to top,rgba(230,255,117,0.50) 40%,transparent 40%);
        }
        #title .container {
            position: relative;
            height: 230px;
        }
        #title .cycle {
            position: absolute;
            top:0;
            right: 0;
            margin: 1rem ;
            height: 60px;
            width: 60px;
            background-color: #eaffd0;
            color: #95e1d3;
            font-size: .8em;
            font-weight: 900;
            letter-spacing: 0mm;
            line-height: 60px;
            text-align: center;
            display: inline-block;
            border-radius: 50%;
            transform: rotate(20deg);
            box-shadow: 0 10px 20px rgba(0,0,0,0.15),0 10px 40px rgba(0,0,0,0.10);
        }
		#content {
			padding: 15px;
            background-color: white;
		}
        .rank {
            position: relative;
            border-radius: 20px;
            box-shadow: 0 10px 20px rgba(0,0,0,0.16),0 10px 40px rgba(0,0,0,0.23);
            margin-bottom: 1.5rem;
            background-color: white;
        }
        .r3 {
            width: 100%;
        }
        
        .rank .img {
            position: relative;
            border-radius: 20px 20px 0 0;
            height: 50%;
            min-height: 300px;
            background-size: cover;
            background-position: center;
        }
        .rank .img h3 {
            position: absolute;
            top:0;
            right: 0;
            margin: 1rem;
            height: 50px;
            width: 50px;
            background-color:#859C98;
            font-weight: 900;
            letter-spacing: 0mm;
            color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 0;
            border-radius: 50%;
            transform: rotate(20deg);
            box-shadow: 0 10px 20px rgba(0,0,0,0.16),0 10px 40px rgba(0,0,0,0.23);
        }
        
        .rank.r3 .img {
            min-height: 400px;
        }
        
        .rank .img .account_icon {
            position: absolute;
            top:0;
            left: 0;
            margin: 1rem;
            height: 60px;
            width: 60px;
            background-color:#859C98;
            font-weight: 900;
            letter-spacing: 0mm;
            color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 0;
            border-radius: 50%;
            border: 2px solid white;
            box-shadow: 0 10px 20px rgba(0,0,0,0.16),0 10px 40px rgba(0,0,0,0.23);
        }
        .rank.r3 .img h3 {
            background-color: #fce38a;
        }
        .rank.r2 .img h3 {
            background-color: #95e1d3;
        }
        .rank .img h2 {
            position: absolute;
            bottom:0;
            right: 0;
            padding: 1.5rem;
            font-size: 2.5rem;
            font-weight: 900;
            line-height: 1;
            text-align: right;
            color: white;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-bottom: 0;
            text-shadow: 0 10px 20px rgba(0,0,0,0.8),0 10px 40px rgba(0,0,0,0.8);
        }
        .rank .comment {
            padding: 1.5rem;
            color: #657786;            
        }
        .rank .comment h3 {
            font-weight: 900;
            margin-bottom: 0;
            color: #f38181;
        }
        .description {
            font-size: 0.6rem;
            line-height: 1.5 !important;
        }
	</style>
    
    <div id="title" class="bold">
        <div class="container">
            <div class="cycle">特選</div>
            <div class="toptext"><h1>今期オススメアニメリスト</h1>
                <div>今期特選のアニメが目白押し！</div>
                <div style="font-size: 0.9rem"><?= date('Y') . '年 / ' . ceil(date('m') / 3). 'クール目' ?></div>
            </div>
        </div>
    </div>

    <?= $ad;?>
<style>
    #timeline {
        overflow-x: scroll;
        -webkit-overflow-scrolling: touch;
        display: inline-block;
        white-space: normal;
        position: relative;
        padding: 0;
        line-height: 1.1;
        letter-spacing: 0.2mm;
    }
    #timeline .info {
        min-width: 150px;
        height: 150px;
        position: relative;
        background-image: linear-gradient(120deg, #fdfbfb 0%, #ebedee 100%);
        background-size: cover!important;
        background-position: center!important;
    }
    #timeline .info .time {
        position: absolute;
        top: 0;
        left: 0;
        padding: 5px;
        background-color: white;
        color: black;
        box-shadow: 0 2px 5px rgba(0,0,0,0.1),0 3px 10px rgba(0,0,0,0.2);
    }
    #timeline .info .ch {
        position: absolute;
        max-width: calc(150px - 53.5px);
        font-size: .7rem;
        text-align: right;
        top: 0;
        right: 0;
        padding: 5px;
        background-color: #fce38a;
        color: white;
        box-shadow: 0 2px 5px rgba(0,0,0,0.1),0 3px 10px rgba(0,0,0,0.2);
    }
    #timeline .info .title {
        position: absolute;
        bottom: 0;
        right: 0;
        padding: 5px;
        text-align: right;
        color: white;
        text-shadow: 0 2px 5px rgba(0,0,0,0.3),0 3px 10px rgba(0,0,0,.7);
    }
    #timeline .infotitle {
        min-width: 150px;
        height: 150px;
        
        position: relative;
        background-color: #95e1d3;
        background-size: cover!important;
        background-position: center!important;
        display: flex;
        justify-content: center;
        align-items: center;
        text-align: center;
    }
    #timeline .infotitle .title {
        color: white;
        font-size: 2rem;
        padding: 20px;
    }
    #timeline .infotitle .title .subtitle {
        color: white;
        font-size: .8rem;
        margin-top: 5px;
        
    }
    #timeline .infotitle .sub {
        position: absolute;
        font-size: .8rem;
        text-align: right;
        top: 0;
        left: 0;
        padding: 5px;
        background-color: #fce38a;
        color: white;
    }
    #timeline .infotitle .time {
        position: absolute;
        top: 0;
        right: 0;
        padding: 5px;
        background-color: white;
        color: black;
    }
</style>
    <div class="d-flex flex-nowrap bold" id="timeline">
        <div class="infotitle">
            <div class="title">
                番組表
                <div class="subtitle normal">
                    放送情報をまるまる1日分お届け！
                </div>
            </div>
            <div class="sub">アニメ</div>
            <div class="time"><?= date('H:i') ?></div>
        </div>
        <?
        
        $linejson = file_get_contents(sprintf('http://cal.syoboi.jp/rss2.php?filter=3&days=1&alt=json'));
        $linejson = json_decode($linejson, true);
            foreach($linejson['items'] as $timedata) {
                $query = "SELECT account_icon,anime_id FROM title WHERE title = '".$timedata['Title']."'";
                
                if ($bgimg = $mysqli->query($query)) {
                    while( $bi = $bgimg->fetch_assoc() ) {
                        echo '<style>#timeline .info.i'.$timedata['TID'].' { background: url("'.str_replace('_normal','',$bi['account_icon']).'"); } </style>';
                        $animeid = $bi['anime_id'];
                        if($bi['anime_id']){
                            echo '<a style="text-decoration: none" href="review.php?anime='.$bi['anime_id'].'">';
                        }
                        else {
                            echo '<a style="text-decoration: none" href="review.php?anime='.$timedata['Urls'].'">';
                        }
                    }
                    $bgimg->close();
                }
                
                echo '<div class="info i'.$timedata['TID'].'">';
                echo '<div class="time">'.date('H:i', $timedata['StTime']).'</div>';
                echo '<div class="ch">'.$timedata['ChName'].'</div>';
                echo '<div class="title">'.$timedata['Title'].'</div>';
                echo '</div>';
                if($animeid) {
                    echo '</a>';
                }
            }
        ?>
    </div>
    <div class="container" id="content">
        <div class="row">
<?

$result = $mysqli->query("SELECT * FROM title WHERE season = '".ceil(date('m') / 3)."' and year = '".date('Y')."' order by rank desc, title asc");
while( $data = $result->fetch_assoc() ) {
    $animeimg = $data['image'];
    
    if($data['rank'] == '3') {
        print('<div class="col-md-12">');
    }
    else {
        print('<div class="col-md-6">');
    }
    
    print ('<a style="text-decoration: none;" href="review.php?anime='. $data['anime_id'] .'">');
    
    print ('<div class="rank r'.$data['rank']);
	print ('">');
    
    if($data['banner']){
        $animeimg = $data['banner'];
    }
    elseif(!$data['banner']) {
        $animeimg = $data['image'];
    }
    print ('<div class="img" style="background-image: url(' . $animeimg . ');">');
    
    if ($data['rank']) {
        print('<h3>');
        if($data['rank'] == '3') {
            echo '1';
        }
        elseif($data['rank'] == '1') {
            echo '3';
        }
        else {
            echo '2';
        }
        print('</h3>');
    }
    
    print ('<img class="account_icon" src="'.str_replace('_normal','',$data['account_icon']).'" alt="">');
    print ('<h2>'.$data['title'].'</h2>');
    print('</div>');
    print('<div class="comment">');
    $result2 = $mysqli->query("SELECT * FROM anime WHERE  anime_id ='".$data['anime_id']."'");
        while( $data2 = $result2->fetch_assoc() ) {
            print('<h3>'.$data2['comment_title'].'</h3>'.$data2['comment_msg']);
            echo '<hr>';
        }
    $result2->close();
    print('<div class="description">'.$data['description'].'</div>');
    print('</div>');
    print('</div>');
    print('</a>');
    print('</div>');
    
}
$result->close();
?>
    </div></div>


<?= $ad;?>
<?

$mysqli->close();
include('common/html/footer.php');
?>