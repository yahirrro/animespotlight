<?php
/**************************************************

	[GET statuses/oembed]のお試しプログラム

	認証方式: ベアラートークン

	配布: SYNCER
	公式ドキュメント: https://dev.twitter.com/rest/reference/get/statuses/oembed
	日本語解説ページ: https://syncer.jp/Web/API/Twitter/REST_API/GET/statuses/oembed/

**************************************************/

// 設定
$bearer_token2 = 'AAAAAAAAAAAAAAAAAAAAAMlD4QAAAAAAWMDrYQ06uLEMnOHMdQr27X3DMDA%3DUtMNHHz2Eh9lnFMEb0kd10e1HE9sb5oL9sw296RVGP4vdTSzzI' ;	// ベアラートークン
$request_url2 = 'https://publish.twitter.com/oembed' ;		// エンドポイント

// パラメータ (オプション)
$params2 = array(
    "url" => "https://twitter.com/yurucamp_anime/status/960441998304952320",
    "maxwidth" => "1920",
    "align" => "center",
) ;

// パラメータがある場合
if( $params2 ) {
    $request_url2 .= '?' . http_build_query( $params2 ) ;
}

// リクエスト用のコンテキスト
$context2 = array(
    'http' => array(
        'method' => 'GET' , // リクエストメソッド
        'header' => array(			  // ヘッダー
            'Authorization: Bearer ' . $bearer_token2 ,
        ) ,
    ) ,
) ;

// [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
$json2 = @file_get_contents( $request_url2 , false , stream_context_create( $context2 ) ) ;

// JSONをオブジェクトに変換 (処理をする場合)
$obj2 = json_decode( $json2,true ) ;
if ( $obj2) {
    echo $obj2['html'];
}