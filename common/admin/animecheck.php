<?php
class OpenGraph implements Iterator
{
	public static $TYPES = array(
		'activity' => array('activity', 'sport'),
		'business' => array('bar', 'company', 'cafe', 'hotel', 'restaurant'),
		'group' => array('cause', 'sports_league', 'sports_team'),
		'organization' => array('band', 'government', 'non_profit', 'school', 'university'),
		'person' => array('actor', 'athlete', 'author', 'director', 'musician', 'politician', 'public_figure'),
		'place' => array('city', 'country', 'landmark', 'state_province'),
		'product' => array('album', 'book', 'drink', 'food', 'game', 'movie', 'product', 'song', 'tv_show'),
		'website' => array('blog', 'website'),
	);
	private $_values = array();

	static public function fetch($URI) {
        $curl = curl_init($URI);

        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 15);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

        $response = curl_exec($curl);

        curl_close($curl);

        if (!empty($response)) {
            return self::_parse($response);
        } else {
            return false;
        }
	}
	static private function _parse($HTML) {
		$old_libxml_error = libxml_use_internal_errors(true);

		$doc = new DOMDocument();
		$doc->loadHTML($HTML);
		
		libxml_use_internal_errors($old_libxml_error);

		$tags = $doc->getElementsByTagName('meta');
		if (!$tags || $tags->length === 0) {
			return false;
		}

		$page = new self();

		$nonOgDescription = null;
		
		foreach ($tags AS $tag) {
			if ($tag->hasAttribute('property') &&
			    strpos($tag->getAttribute('property'), 'og:') === 0) {
				$key = strtr(substr($tag->getAttribute('property'), 3), '-', '_');
				$page->_values[$key] = $tag->getAttribute('content');
			}
			
			if ($tag ->hasAttribute('value') && $tag->hasAttribute('property') &&
			    strpos($tag->getAttribute('property'), 'og:') === 0) {
				$key = strtr(substr($tag->getAttribute('property'), 3), '-', '_');
				$page->_values[$key] = $tag->getAttribute('value');
			}
			
			if ($tag->hasAttribute('name') && $tag->getAttribute('name') === 'description') {
                $nonOgDescription = $tag->getAttribute('content');
            }
			
		}
		
		if (!isset($page->_values['title'])) {
            $titles = $doc->getElementsByTagName('title');
            if ($titles->length > 0) {
                $page->_values['title'] = $titles->item(0)->textContent;
            }
        }
        if (!isset($page->_values['description']) && $nonOgDescription) {
            $page->_values['description'] = $nonOgDescription;
        }
        if (!isset($page->values['image'])) {
            $domxpath = new DOMXPath($doc);
            $elements = $domxpath->query("//link[@rel='image_src']");

            if ($elements->length > 0) {
                $domattr = $elements->item(0)->attributes->getNamedItem('href');
                if ($domattr) {
                    $page->_values['image'] = $domattr->value;
                    $page->_values['image_src'] = $domattr->value;
                }
            }
        }

		if (empty($page->_values)) { return false; }
		
		return $page;
	}
	public function __get($key) {
		if (array_key_exists($key, $this->_values)) {
			return $this->_values[$key];
		}
		
		if ($key === 'schema') {
			foreach (self::$TYPES AS $schema => $types) {
				if (array_search($this->_values['type'], $types)) {
					return $schema;
				}
			}
		}
	}
	public function keys() {
		return array_keys($this->_values);
	}
	public function __isset($key) {
		return array_key_exists($key, $this->_values);
	}

	public function hasLocation() {
		if (array_key_exists('latitude', $this->_values) && array_key_exists('longitude', $this->_values)) {
			return true;
		}
		
		$address_keys = array('street_address', 'locality', 'region', 'postal_code', 'country_name');
		$valid_address = true;
		foreach ($address_keys AS $key) {
			$valid_address = ($valid_address && array_key_exists($key, $this->_values));
		}
		return $valid_address;
	}
	private $_position = 0;
	public function rewind() { reset($this->_values); $this->_position = 0; }
	public function current() { return current($this->_values); }
	public function key() { return key($this->_values); }
	public function next() { next($this->_values); ++$this->_position; }
	public function valid() { return $this->_position < sizeof($this->_values); }
}





$mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
if( $mysqli->connect_errno ) {
	echo $mysqli->connect_errno . ' : ' . $mysqli->connect_error;
}

$mysqli->set_charset('utf8');

if(isset($_GET['cours'])) {
    $cours = $_GET['cours'];
}
else {
    $cours = ceil(date('m') / 3);
}
if(isset($_GET['year'])) {
    $year = $_GET['year'];
}
else {
    $year = date('Y');
}
echo $cours.'  '.$year;
$list = file_get_contents(sprintf('https://api.moemoe.tokyo/anime/v1/master/%d/%d', date('Y'), $cours));
$list = json_decode($list, true);

/***** 設定項目 *****/
$api_key = "dLzGiyaJBZoD9q7QZHI7uAunK" ;	// APIキー
$api_secret = "HCR5dnw8sgL4zYYxDaMSpCNSeBTpTVikR1U6nFmVmPKBTM9Ipa" ;	// APIシークレット
$access_token = "1873618208-vGCtJzUu0oWOVf7CUiN45xnQglX1hx6G96H9NiJ" ;	// アクセストークン
$access_token_secret = "V2Qv6NNfT8Ux06OsVBGDZ6YRRemUrd383CVxDABbzIO6W" ;


foreach ($list as $adata) {
    $graph = OpenGraph::fetch($adata['public_url']);

    $image = $graph->image;
    $description = $graph->description;
    
    /***** プログラムの実行 *****/
    // ユーザータイムラインを取得する
    list( $response_body, $response_header ) = syncer_twitter_rest_api_request ( $api_key, $api_secret, $access_token, $access_token_secret, "https://api.twitter.com/1.1/users/show.json", "GET", [
        "screen_name" => $adata['twitter_account'] ,
    ] ) ;
    // 取得したプロフィール画像をサイズ別に表示
    $user_object = json_decode( $response_body, true ) ;
    
    if(!isset($image)) {
        if ( $user_object ) {
            $image = $user_object["profile_banner_url"];
        }
    }
    
    if(!isset($description) or strpos($description,'ã') !== false) {
        if ( $user_object ) {
            $description = $user_object["description"];
        }
    }
    
    if ( $user_object ) {
        $account_icon = $user_object["profile_image_url_https"];
    }
    
    $asset = "INSERT INTO title (title,public_url,image,description,account,account_icon,hashtag,year,season) VALUES ('".$adata['title']."','".$adata['public_url']."','".$image."','".$description."','".$adata['twitter_account']."','".$account_icon."','".$adata['twitter_hash_tag']."','".date('Y')."','".$cours."') ON DUPLICATE KEY UPDATE account_icon ='".$account_icon."', description ='".$description."', account ='".$adata['twitter_account']."', image ='".$image."', hashtag ='".$adata['twitter_hash_tag']."', title ='".$adata['title']."', public_url='".$adata['public_url']."'";
    $mysqli->query($asset);
    
    $description = '';
}

function syncer_twitter_rest_api_request ( $api_key="", $api_secret="", $access_token="", $access_token_secret="", $request_url="", $request_method="", $params_a=[] ) {
	$request_headers = [] ;
	$request_body = "" ;

	$params_b = array(
		'oauth_token' => $access_token ,
		'oauth_consumer_key' => $api_key ,
		'oauth_signature_method' => 'HMAC-SHA1' ,
		'oauth_timestamp' => time() ,
		'oauth_nonce' => microtime() ,
		'oauth_version' => '1.0' ,
	) ;

	switch ( $request_method ) {
		case "POST" :
			switch( $request_url ) {
				case( 'https://api.twitter.com/1.1/account/update_profile_background_image.json' ) :
				case( 'https://api.twitter.com/1.1/account/update_profile_image.json' ) :
					$media_param = 'image' ;
				break ;

				case( 'https://api.twitter.com/1.1/account/update_profile_banner.json' ) :
					$media_param = 'banner' ;
				break ;

				case( 'https://upload.twitter.com/1.1/media/upload.json' ) :
					$media_param = ( isset($params_a['media']) && !empty($params_a['media']) ) ? 'media' : 'media_data' ;
				break ;
			}

			// multipart POST
			if ( isset($media_param) && isset($params_a[ $media_param ]) ) {
				$media_data = ( $params_a[ $media_param ] ) ? $params_a[ $media_param ] : "" ;

				if( isset( $params_a[ $media_param ] ) ) unset( $params_a[ $media_param ] ) ;

				$boundary = 's-y-n-c-e-r---------------' . md5( mt_rand() ) ;

				$request_body .= '--' . $boundary . "\r\n" ;
				$request_body .= 'Content-Disposition: form-data; name="' . $media_param . '"; ' ;
				$request_body .= "\r\n" ;
				$request_body .= "\r\n" . $media_data . "\r\n" ;

				foreach( $params_a as $key => $value ) {
					$request_body .= '--' . $boundary . "\r\n" ;
					$request_body .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n" ;
					$request_body .= $value . "\r\n" ;
				}

				$request_body .= '--' . $boundary . '--' . "\r\n\r\n" ;

				$request_headers[] = "Content-Type: multipart/form-data; boundary=" . $boundary ;

				$params_c = $params_b ;

			// POST
			} else {
				switch ( $request_url ) {
					case "https://api.twitter.com/1.1/collections/entries/curate.json" :
						$params_c = $params_b ;
						$request_body = $params_a ;
					break ;

					default :
						$params_c = array_merge( $params_a , $params_b ) ;

						if ( $params_a ) {
							$request_body = http_build_query( $params_a ) ;
						}
					break ;
				}
			}
		break ;

		// GET
		case "GET" :
			$params_c = array_merge( $params_a , $params_b ) ;
		break ;
	}

	ksort( $params_c ) ;

	$signature_key = rawurlencode( $api_secret ) . '&' . rawurlencode( $access_token_secret ) ;

	$request_params = http_build_query( $params_c, '', '&' ) ;
	$request_params = str_replace( array( '+', '%7E' ) , array( '%20', '~' ) , $request_params ) ;
	$request_params = rawurlencode( $request_params ) ;
	$encoded_request_method = rawurlencode( $request_method ) ;
	$encoded_request_url = rawurlencode( $request_url ) ;
	$signature_data = $encoded_request_method . '&' . $encoded_request_url . '&' . $request_params ;
	$hash = hash_hmac( 'sha1' , $signature_data , $signature_key , TRUE ) ;
	$signature = base64_encode( $hash ) ;
	$params_c['oauth_signature'] = $signature ;
	$header_params = http_build_query( $params_c , '' , ',' ) ;

	$context = array(
		'http' => array(
			'method' => $request_method ,
			'header' => array(
				'Authorization: OAuth ' . $header_params ,
			) ,
			'content' => $request_body ,
		) ,
	) ;

	if ( $request_headers ) {
		$context['http']['header'] = array_merge( $context['http']['header'], $request_headers ) ;
	}

	if( $request_method == "GET" ) {
		$request_url .= '?' . http_build_query( $params_a ) ;
	}

	$curl = curl_init() ;
	curl_setopt( $curl, CURLOPT_URL , $request_url ) ;
	curl_setopt( $curl, CURLOPT_HEADER, true ) ; 
	curl_setopt( $curl, CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;
	curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER , false ) ;
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER , true ) ;
	curl_setopt( $curl, CURLOPT_HTTPHEADER , $context['http']['header'] ) ;
	if ( isset($context['http']['content']) ) {
		curl_setopt( $curl, CURLOPT_POSTFIELDS , $context['http']['content'] ) ;
	}
	curl_setopt( $curl, CURLOPT_TIMEOUT, 5 ) ;
	$res1 = curl_exec( $curl ) ;
	$res2 = curl_getinfo( $curl ) ;
	curl_close( $curl ) ;

	$response_body = substr( $res1, $res2['header_size'] ) ;
	$response_header = substr( $res1, 0, $res2['header_size'] ) ;

	return [ $response_body, $response_header ] ;
}

$asset = "INSERT INTO crawler (category) VALUES ('".$year." - ".$cours."') ON DUPLICATE KEY UPDATE category = '".$year." - ".$cours."', update_time = '".date("Y-m-d H:i:s")."'";
$mysqli->query($asset);

$mysqli->close();
?>