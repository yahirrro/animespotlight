    <footer>
        <div class="info">
            <div class="container" style="font-size: 0.7rem">
                当サイト上で使用しているメディアの著作権ついては、各著作権者へ帰還します。当サイトは公式サイトや各種SNSから画像や文章を自動的に取得します。当サイトのサーバー上には一切画像等は保存されておりません。<br>
                番組情報は<a href="https://qiita.com/AKB428/items/64938febfd4dcf6ea698" style="text-decoration: none;">ShangriLa Anime API</a>から提供されています。情報に誤りがありましたらご連絡ください。<br>
                <?
                $mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
                if( $mysqli->connect_errno ) {
                echo $mysqli->connect_errno . ' : ' . $mysqli->connect_error;
                }
                $mysqli->set_charset('utf8');
                if($_SERVER["REQUEST_URI"] == '/'){
                    $result = $mysqli->query("SELECT * FROM crawler WHERE category = '".date('Y')." - ".ceil(date('m') / 3)."'");
                    while( $data = $result->fetch_assoc() ) {
                        echo '最終チェック : '.$data['update_time'];
                    }
                    
                }
                ?>
            </div>
            
        </div>
        <style>
            .url {
                padding: 5px 7px;
                border-right: solid 1px white;
                color: white;
            }
            .link {
                background-color:#C7EFE7;
                padding: .5rem 0;
            }
        </style>
        <div class="link">
            <div class="container">
                <div class="d-flex flex-nowrap bold" id="url">
                    
                    <?
                    
                    $result = $mysqli->query("SELECT * FROM note order by note_id asc");
                    while( $data = $result->fetch_assoc() ) {
                        echo '<a href="https://anime.spotlight.tokyo/note.php?id='.$data['note_id'].'">'.'<div class="url">'.$data['title'].'</div></a>';
                    }
                    $mysqli->close();
                    
                    ?>
                </div>
            </div>
        </div>
        <div class="copyright bold">
            <div class="container">
                <div style="margin-top: 5px;">
                    &copy; Spotlight.tokyo (anime) / アニメデイズ!
                </div>
            </div>
        </div>
    </footer>
	<script>
	  (function(d) {
		var config = {
		  kitId: 'jrs7ply',
		  scriptTimeout: 3000,
		  async: true
		},
		h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
	  })(document);
	</script>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>