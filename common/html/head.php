<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,shrink-to-fit=no">
    
	<title><?= $pagetitle ?></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://anime.spotlight.tokyo/common/html/artwork.css">
    <link rel="shortcut icon" href="https://anime.spotlight.tokyo/common/picture/icon.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://anime.spotlight.tokyo/common/picture/apple-touch-icon.png">
    <? if ($twmeta == '') {
		echo '
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@Yahimotto" />
		<meta name="twitter:title" content="'.$pagetitle.'" />
		<meta name="twitter:description" content="'.$subtitle.'" />
		<meta name="twitter:image" content="https://anime.spotlight.tokyo/common/picture/twimg.png" />
		';
		}
		else {
			echo $twmeta;
		}
	?>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
      (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-6248776021404303",
        enable_page_level_ads: true
      });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114291154-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-114291154-1');
    </script>

</head>
<body>
    <div class="fixed-top">
        <a href="https://anime.spotlight.tokyo" class="site bold" style="color: black;text-decoration: none;">
            アニメデイズ！
        </a>
    </div>
    <?
    if (isset($user->id_str)) {
        echo '
        <div class="fixed-bottom">
        <a href="https://anime.spotlight.tokyo/dashboad" class="site bold" style="background-color:#fce38a;color: white;text-decoration: none;">ログインしています</a></div>';
    }
    ?>