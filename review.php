<?
session_start();
require_once 'common/login/common.php';
require_once 'common/login/twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$user = $connection->get("account/verify_credentials");

$mysqli = new mysqli('localhost', 'hash_anime', 'animelists', 'hash_anime');
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
$result = $mysqli->query("SELECT * FROM review WHERE anime_id = '".$_GET['anime']."'");
while( $data = $result->fetch_assoc() ) {
    $body = $data['body'];
}
$result = $mysqli->query("SELECT * FROM anime WHERE anime_id = '".$_GET['anime']."'");
while( $data = $result->fetch_assoc() ) {
    $animecommenttitle = $data['comment_title'];
    $animecommentmsg = $data['comment_msg'];
    $animereview = $data['review'];
}
$result = $mysqli->query("SELECT * FROM title WHERE anime_id = '".$_GET['anime']."'");
while( $data = $result->fetch_assoc() ) {
    $animetitle = $data['title'];
    $animeimg = $data['image'];
    $animebanner = $data['banner'];
    $animeurl = $data['public_url'];
    $animeaccount = $data['account'];
    $animehashtag = $data['hashtag'];
    $animedescription = $data['description'];
    $account_icon = $data['account_icon'];
}
$animeimage = $animeimg;
if($animebanner) {
    $animeimage = $animebanner;
}
elseif(!$animebanner) {
    $animeimage = $animeimg;
}

$mysqli->close();
if (!isset($animetitle)) {
    echo '<meta http-equiv="refresh" content="0; URL=https://anime.spotlight.tokyo/404.php">';
}

$pagetitle = $animetitle .' "'.$animecommenttitle.'" / アニメデイズ!';
if(!isset($animecommenttitle)){
    $pagetitle = $animetitle .' / アニメデイズ!';
}
$subtitle = $animetitle .'の情報をすべてまとめて掲載しています！ / アニメデイズ!';

// 設定
$bearer_token = 'AAAAAAAAAAAAAAAAAAAAAMlD4QAAAAAAWMDrYQ06uLEMnOHMdQr27X3DMDA%3DUtMNHHz2Eh9lnFMEb0kd10e1HE9sb5oL9sw296RVGP4vdTSzzI' ;	// ベアラートークン
$request_url = 'https://api.twitter.com/1.1/statuses/user_timeline.json' ;		// エンドポイント

// パラメータ (オプション)
$params = array(
    "screen_name" => $animeaccount,
    "count" => "4"
) ;

// パラメータがある場合
if( $params ) {
    $request_url .= '?' . http_build_query( $params ) ;
}

// リクエスト用のコンテキスト
$context = array(
    'http' => array(
        'method' => 'GET' , // リクエストメソッド
        'header' => array(			  // ヘッダー
            'Authorization: Bearer ' . $bearer_token ,
        ) ,
    ) ,
) ;

// cURLを使ってリクエスト
$curl = curl_init() ;
curl_setopt( $curl , CURLOPT_URL , $request_url ) ;
curl_setopt( $curl , CURLOPT_HEADER, 1 ) ; 
curl_setopt( $curl , CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;			// メソッド
curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false ) ;								// 証明書の検証を行わない
curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true ) ;								// curl_execの結果を文字列で返す
curl_setopt( $curl , CURLOPT_HTTPHEADER , $context['http']['header'] ) ;			// ヘッダー
curl_setopt( $curl , CURLOPT_TIMEOUT , 5 ) ;										// タイムアウトの秒数
$res1 = curl_exec( $curl ) ;
$res2 = curl_getinfo( $curl ) ;
curl_close( $curl ) ;

// 取得したデータ
$json = substr( $res1, $res2['header_size'] ) ;				// 取得したデータ(JSONなど)
$header = substr( $res1, 0, $res2['header_size'] ) ;		// レスポンスヘッダー (検証に利用したい場合にどうぞ)

// [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
// $json = @file_get_contents( $request_url , false , stream_context_create( $context ) ) ;

// JSONをオブジェクトに変換 (処理をする場合)
$obj = json_decode( $json,true ) ;





include('common/html/head.php');
include('common/html/ad.php');
?>
<style>
    #img {
        height: 70vh;
        min-height: 200px;
        background-color: #fce38a;
        background-size: cover;
        background-position: center;
        position: relative;
        z-index: 3;
    }
    #img .profile {
        position: absolute;
        bottom:0;
        left: 0;
        color: white;
        padding: 1.5rem;
        text-shadow: 0 10px 20px rgba(0,0,0,0.8),0 10px 40px rgba(0,0,0,0.8);
    }
    #img .profile h2 {
        font-size: 1.5rem;
        font-weight: 900;
    }
    #img .account_icon {
        position: absolute;
        top:0;
        right: 0;
        margin: 1.5rem;
        height: 80px;
        width: 80px;
        background-color:#859C98;
        font-weight: 900;
        letter-spacing: 0mm;
        color: white;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-bottom: 0;
        border-radius: 50%;
        border: 4px solid white;
        box-shadow: 0 10px 20px rgba(0,0,0,0.16),0 10px 40px rgba(0,0,0,0.23);
    }
    #content {
        padding-top: 1.5rem;
        padding-bottom: 1.5rem;
    }
    .info a.site,.info a.site:hover {
        padding: .5rem;
        background-color: #fce38a;
        color: white;
        text-decoration: none;
        margin-right: .5rem;
        margin-top: 1rem;
        margin-bottom: 1rem;
        box-shadow: 0 10px 20px rgba(0,0,0,0.15),0 10px 40px rgba(0,0,0,0.10);
    }
    .info .hashtag,.info .account,.sharetw,.sharetw:hover {
        padding: .5rem;
        background-color: #1da1f2;
        color: white;
        text-decoration: none;
        margin-right: .5rem;
        margin-bottom: 1rem;
        box-shadow: 0 10px 20px rgba(0,0,0,0.15),0 10px 40px rgba(0,0,0,0.10);
    }
    .sharetw,.sharetw:hover {
        float: right;
        margin: 0;
        margin-top: 1.3rem;
        letter-spacing: 0.2mm;
    }
    .stream {
        padding: 15px;
        background-color: white;
        border:1px solid #e6ecf0;
        margin-bottom: 15px;        
    }
    .post {
        margin-left: 50px;
        position: relative;
    }
    .item {
        margin: 0;
        display: flex;
        flex-wrap:wrap;
    }
    .item a,.item a:hover {
        text-decoration: none;
    }
    .item .avatar {
        position: absolute;
        float: left;
        width: 40px;
        height: 40px;
        border-radius: 50%;
    }
    .userinfo {
        display: inline-block;
        width: 100%;
        margin-bottom: 5px;
        letter-spacing: 0.5mm;
        line-height: 1.3;
    }
    .usericon {
        margin-left: -50px;
    }
    .username {
        display: inline-block;
        margin-bottom: 0;
        font-size: 13px;
        padding-right: 8px;
        color: #14171a;
    }
    .userid,.posttime {
        font-size: 10px;
        font-weight: normal;
        color:#657786;
        padding-right: 8px;
    }
    .text {
        margin-bottom: 0;
        display: flex;
        color: black;
    }
    a .text,a .text:hover {
        text-decoration: none;
    }
    .tag {
        padding: 0 5px;
        background-image: linear-gradient(to top,rgba(230,255,117,0.50) 40%,transparent 40%);
    }
    .times {
        padding: 0 5px;
        background-image: linear-gradient(to top,rgba(84,241,255,0.50) 40%,transparent 40%);
    }
</style>
<div class="fixed-top" style="z-index: 2 !important">
    <div class="titlenav">
        <span class="site title bold">
            <?= $animetitle ?>
        </span>
    </div>
</div>
<div id="img" style="background-image: url('<?= $animeimage ?>')">
    <div class="profile">
        <?
            if(isset($animecommenttitle)) {
                echo '<h1>'. $animecommenttitle .'</h1>';
            }
        ?>
        
        <h2><?= $animetitle ?></h2>
    </div>
    <? 
    print ('<img class="account_icon" src="'.str_replace('_normal','',$account_icon).'" alt="">');
    ?>
</div>
<?= $ad;?>
<div id="content" class="container">
    <?
        if(isset($animecommentmsg)) {
            echo '<h2 style="font-size:1.6rem">'. $animecommentmsg .'</h2>';
        }
    ?>
    <?= $animereview ?>
    
    <? 
    if(isset($animecommentmsg)) {
        echo '<hr>';
    }
    ?>       
        
    
    <?
    if(isset($animeurl)) {
        echo '
        <div class="info">
            <h3>概要</h3>
            '.$animedescription.'<br>
            <div style="text-align:right; font-size:.8rem;color:#657786;">公式サイトより</div>
            <hr>
            ';
            
            
        echo '
            <a href="' . $animeurl . '" class="site">公式サイト</a>
            <a href="https://twitter.com/hashtag/' . $animehashtag . '" class="hashtag">ハッシュタグ</a>
            <a href="https://twitter.com/' . $animeaccount . '" class="account">アカウント</a>
        </div>
        <hr>
        <h3>最近のツイート</h3>
        <div class="row">';
        foreach ($obj as $tweet) {
            $bearer_token2 = 'AAAAAAAAAAAAAAAAAAAAAMlD4QAAAAAAWMDrYQ06uLEMnOHMdQr27X3DMDA%3DUtMNHHz2Eh9lnFMEb0kd10e1HE9sb5oL9sw296RVGP4vdTSzzI' ;	// ベアラートークン
            $request_url2 = 'https://publish.twitter.com/oembed' ;		// エンドポイント
            // パラメータ (オプション)
            $params2 = array(
                "url" => 'https://twitter.com/'.$animeaccount.'/status/'.$tweet['id_str'],
                "maxwidth" => "1920",
                "align" => "center",
            ) ;

            // パラメータがある場合
            if( $params2 ) {
                $request_url2 .= '?' . http_build_query( $params2 ) ;
            }

            // リクエスト用のコンテキスト
            $context2 = array(
                'http' => array(
                    'method' => 'GET' , // リクエストメソッド
                    'header' => array(			  // ヘッダー
                        'Authorization: Bearer ' . $bearer_token2 ,
                    ) ,
                ) ,
            ) ;

            // [cURL]ではなく、[file_get_contents()]を使うには下記の通りです…
            $json2 = @file_get_contents( $request_url2 , false , stream_context_create( $context2 ) ) ;

            // JSONをオブジェクトに変換 (処理をする場合)
            $obj2 = json_decode( $json2,true ) ;
            
            echo '
            <div class="col-md-6">';
            if ($obj2) {
                echo $obj2['html'];
            }
            echo '
            </div>';
        }
        echo '<div style="text-align:right; width:100%;"><a class="sharetw" href="http://twitter.com/share?text=「'.$animetitle.'」＃'.$animehashtag .' の情報をアニメデイズで確認しよう！ '.(empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"].'">Twitterで共有</a></div></div>';
    }
    ?>
</div>

<?
function syncer_twitter_rest_api_request ( $api_key="", $api_secret="", $access_token="", $access_token_secret="", $request_url="", $request_method="", $params_a=[] ) {
	$request_headers = [] ;
	$request_body = "" ;

	$params_b = array(
		'oauth_token' => $access_token ,
		'oauth_consumer_key' => $api_key ,
		'oauth_signature_method' => 'HMAC-SHA1' ,
		'oauth_timestamp' => time() ,
		'oauth_nonce' => microtime() ,
		'oauth_version' => '1.0' ,
	) ;

	switch ( $request_method ) {
		case "POST" :
			switch( $request_url ) {
				case( 'https://api.twitter.com/1.1/account/update_profile_background_image.json' ) :
				case( 'https://api.twitter.com/1.1/account/update_profile_image.json' ) :
					$media_param = 'image' ;
				break ;

				case( 'https://api.twitter.com/1.1/account/update_profile_banner.json' ) :
					$media_param = 'banner' ;
				break ;

				case( 'https://upload.twitter.com/1.1/media/upload.json' ) :
					$media_param = ( isset($params_a['media']) && !empty($params_a['media']) ) ? 'media' : 'media_data' ;
				break ;
			}

			// multipart POST
			if ( isset($media_param) && isset($params_a[ $media_param ]) ) {
				$media_data = ( $params_a[ $media_param ] ) ? $params_a[ $media_param ] : "" ;

				if( isset( $params_a[ $media_param ] ) ) unset( $params_a[ $media_param ] ) ;

				$boundary = 's-y-n-c-e-r---------------' . md5( mt_rand() ) ;

				$request_body .= '--' . $boundary . "\r\n" ;
				$request_body .= 'Content-Disposition: form-data; name="' . $media_param . '"; ' ;
				$request_body .= "\r\n" ;
				$request_body .= "\r\n" . $media_data . "\r\n" ;

				foreach( $params_a as $key => $value ) {
					$request_body .= '--' . $boundary . "\r\n" ;
					$request_body .= 'Content-Disposition: form-data; name="' . $key . '"' . "\r\n\r\n" ;
					$request_body .= $value . "\r\n" ;
				}

				$request_body .= '--' . $boundary . '--' . "\r\n\r\n" ;

				$request_headers[] = "Content-Type: multipart/form-data; boundary=" . $boundary ;

				$params_c = $params_b ;

			// POST
			} else {
				switch ( $request_url ) {
					case "https://api.twitter.com/1.1/collections/entries/curate.json" :
						$params_c = $params_b ;
						$request_body = $params_a ;
					break ;

					default :
						$params_c = array_merge( $params_a , $params_b ) ;

						if ( $params_a ) {
							$request_body = http_build_query( $params_a ) ;
						}
					break ;
				}
			}
		break ;

		// GET
		case "GET" :
			$params_c = array_merge( $params_a , $params_b ) ;
		break ;
	}

	ksort( $params_c ) ;

	$signature_key = rawurlencode( $api_secret ) . '&' . rawurlencode( $access_token_secret ) ;

	$request_params = http_build_query( $params_c, '', '&' ) ;
	$request_params = str_replace( array( '+', '%7E' ) , array( '%20', '~' ) , $request_params ) ;
	$request_params = rawurlencode( $request_params ) ;
	$encoded_request_method = rawurlencode( $request_method ) ;
	$encoded_request_url = rawurlencode( $request_url ) ;
	$signature_data = $encoded_request_method . '&' . $encoded_request_url . '&' . $request_params ;
	$hash = hash_hmac( 'sha1' , $signature_data , $signature_key , TRUE ) ;
	$signature = base64_encode( $hash ) ;
	$params_c['oauth_signature'] = $signature ;
	$header_params = http_build_query( $params_c , '' , ',' ) ;

	$context = array(
		'http' => array(
			'method' => $request_method ,
			'header' => array(
				'Authorization: OAuth ' . $header_params ,
			) ,
			'content' => $request_body ,
		) ,
	) ;

	if ( $request_headers ) {
		$context['http']['header'] = array_merge( $context['http']['header'], $request_headers ) ;
	}

	if( $request_method == "GET" ) {
		$request_url .= '?' . http_build_query( $params_a ) ;
	}

	$curl = curl_init() ;
	curl_setopt( $curl, CURLOPT_URL , $request_url ) ;
	curl_setopt( $curl, CURLOPT_HEADER, true ) ; 
	curl_setopt( $curl, CURLOPT_CUSTOMREQUEST , $context['http']['method'] ) ;
	curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER , false ) ;
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER , true ) ;
	curl_setopt( $curl, CURLOPT_HTTPHEADER , $context['http']['header'] ) ;
	if ( isset($context['http']['content']) ) {
		curl_setopt( $curl, CURLOPT_POSTFIELDS , $context['http']['content'] ) ;
	}
	curl_setopt( $curl, CURLOPT_TIMEOUT, 5 ) ;
	$res1 = curl_exec( $curl ) ;
	$res2 = curl_getinfo( $curl ) ;
	curl_close( $curl ) ;

	$response_body = substr( $res1, $res2['header_size'] ) ;
	$response_header = substr( $res1, 0, $res2['header_size'] ) ;

	return [ $response_body, $response_header ] ;
}
echo $ad;
include('common/html/footer.php');
?>